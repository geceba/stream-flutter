import 'package:flutter/material.dart';

class CardWidget extends StatelessWidget {
  final double price;
  final String days;

  const CardWidget({Key key, this.price, this.days}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.white,
      ),
      margin: EdgeInsets.only(right: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            margin: EdgeInsets.only(bottom: 5),
            child: CircleAvatar(
              child: Container(),
            ),
          ),
          Text('${this.price}'),
          Text(
            '${this.days}',
            style: TextStyle(color: Colors.grey),
          ),
        ],
      ),
    );
  }
}
