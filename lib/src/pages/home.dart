import 'package:design/src/widgets/header_widgtet.dart';
import 'package:design/src/widgets/planning_widget.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            child: Column(
              children: [HeaderHome(), Planning()],
            ),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            height: kToolbarHeight,
            child: Container(
              color: Colors.white.withOpacity(0.7),
              child: Row(
                children: [
                  Expanded(
                    child: Icon(Icons.home),
                  ),
                  Expanded(
                    child: Icon(Icons.favorite_border),
                  ),
                  Expanded(
                    child: Icon(Icons.shopping_cart),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
