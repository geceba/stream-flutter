import 'package:flutter/material.dart';
import 'package:design/src/widgets/circle_widget.dart';

class HeaderHome extends StatelessWidget {
  const HeaderHome({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      width: double.infinity,
      height: 200,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [Color(0xff36D1B2), Color(0xff98E1A9)],
          begin: Alignment.bottomCenter,
          end: Alignment.topCenter,
        ),
      ),
      child: Stack(
        children: [
          Positioned(
            left: -size.width * 0.25,
            top: -size.width * 0.33,
            child: Circle(
              radius: size.width * 0.33,
            ),
          ),
          Positioned(
            left: -size.width * .05,
            top: -size.width * 0.50,
            child: Circle(
              radius: size.width * 0.33,
            ),
          ),
          Positioned(
            right: -size.width * .2,
            bottom: -size.width * 0.45,
            child: Circle(
              radius: size.width * 0.33,
            ),
          ),
          Positioned(
            right: -size.width * .48,
            bottom: -size.width * 0.2,
            child: Circle(
              radius: size.width * 0.33,
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 40),
            padding: EdgeInsets.symmetric(horizontal: 15),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  child: CircleAvatar(
                    radius: 25,
                    backgroundImage: AssetImage('assets/bochi.jpg'),
                  ),
                ),
                Container(
                  child: RaisedButton(
                    color: Colors.white,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0)),
                    onPressed: () {},
                    child: Text(
                      'Payday in a week',
                      style: TextStyle(fontSize: 16, color: Color(0xff74D99D)),
                    ),
                  ),
                )
              ],
            ),
          ),
          Positioned(
            bottom: 0,
            child: Container(
              margin: EdgeInsets.all(15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Total balance to spend",
                    style: TextStyle(color: Colors.white, fontSize: 16),
                  ),
                  Container(
                    height: 10,
                  ),
                  Text(
                    "\$5785.55",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 28,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
