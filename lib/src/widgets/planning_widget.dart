import 'package:design/src/widgets/planning_slider.dart';
import 'package:flutter/material.dart';

class Planning extends StatelessWidget {
  const Planning({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 30),
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.symmetric(horizontal: 15),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  child: Text(
                    'Planning Ahead',
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                ),
                Container(
                  child: Row(
                    children: [
                      Text('\$540.52'),
                      Container(
                        margin: EdgeInsets.only(left: 10),
                        child: Icon(
                          Icons.chevron_right,
                          color: Colors.grey,
                          size: 18.0,
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
          PlanningSlide(),
          Container(
            margin: EdgeInsets.symmetric(vertical: 20),
            width: double.infinity,
            child: Divider(),
          )
        ],
      ),
    );
  }
}
