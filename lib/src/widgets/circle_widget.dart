import 'package:flutter/material.dart';

class Circle extends StatelessWidget {
  final double radius;

  const Circle({Key key, this.radius}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: radius * 2,
      height: radius * 2,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(this.radius),
        color: Color(0xffffffe4).withOpacity(0.3),
      ),
    );
  }
}
