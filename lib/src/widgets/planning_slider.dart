import 'package:flutter/material.dart';

import 'card_widget.dart';

class PlanningSlide extends StatelessWidget {
  const PlanningSlide({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.all(15),
      height: 120,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: [
          CardWidget(
            price: 152.34,
            days: 'In a 2 days',
          ),
          CardWidget(
            price: 252.34,
            days: 'In a 2 days',
          ),
          CardWidget(
            price: 52.34,
            days: 'In a 2 days',
          ),
          CardWidget(
            price: 192.34,
            days: 'In a 2 days',
          ),
          CardWidget(
            price: 122.34,
            days: 'In a 2 days',
          ),
        ],
      ),
    );
  }
}
